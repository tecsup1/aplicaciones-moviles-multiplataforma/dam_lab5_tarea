// react navigation:
import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator, HeaderBackground} from '@react-navigation/stack';

import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
} from 'react-native';

import DetailScreen from './components/DetailScreen';
import ListScreen from './components/ListScreen';
import LoginScreen from './components/LoginScreen';
import {TouchableOpacity} from 'react-native-gesture-handler';

const Stack = createStackNavigator();

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="LoginScreen">
          <Stack.Screen
            name="DetailScreen"
            component={DetailScreen}
            options={{
              headerTitle: '',
              headerStyle: {
                backgroundColor: '#FF0315',
              },
              headerRight: props => (
                <Image
                  style={{width: 110, height: 50, flex: 1, margin: 4}}
                  source={require('./images/image1.png')}
                />
              ),
              headerBackImage: props => (
                <Image
                  style={{width: 30, height: 30}}
                  source={require('./images/arrow.png')}
                />
              ),
            }}
          />

          <Stack.Screen
            name="ListScreen"
            component={ListScreen}
            options={{
              headerLeft: () => (
                <TouchableOpacity onPress={() => console.log('holi')}>
                  <Image
                    style={{width: 20, height: 20, marginHorizontal: 10}}
                    source={require('./images/home.png')}
                  />
                </TouchableOpacity>
              ),
              headerTitle: props => (
                <Text style={{fontSize: 25, color: 'white'}}>Sixties</Text>
              ),
              headerTitleAlign: 'center',
              headerRight: props => (
                <Image
                  style={{width: 110, height: 50, flex: 1, margin: 4}}
                  source={require('./images/image1.png')}
                />
              ),
              headerStyle: {
                backgroundColor: '#FF0315',
              },
            }}
          />

          <Stack.Screen
            name="LoginScreen"
            component={LoginScreen}
            options={{headerShown: false}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

const styles = StyleSheet.create({});

export default App;
