import React, {Component} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  Image,
  TextInput,
  ImageBackground,
} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: '',
      password: '',
      fetchingLogin: false,
      loginMessage: '',
    };
  }

  changeUserInput = text => {
    this.setState({user: text});
  };

  changePassInput = text => {
    this.setState({password: text});
  };

  login = item => {
    this.setState({
      fetchingLogin: true,
      loginMessage: 'Cargando...',
    });

    // const url = `http://192.168.0.101:3000/login_lab5_dam?id=${
    const url = `https://lab4-dawa.herokuapp.com/login_lab5_dam?id=${
      this.state.user
    }&pass=${this.state.password}`;

    fetch(url, {method: 'POST'})
      .then(res => res.json())
      .then(resJson => {
        console.log(resJson.result);
        this.setState({
          fetchingLogin: false,
        });
        if (resJson.result) {
          this.props.navigation.navigate('ListScreen');
        } else {
          this.setState({
            loginMessage: 'Sus credenciales son erroneas',
          });
        }
      })
      .catch(error => {
        console.warn(error);
      });
  };

  render() {
    return (
      <ImageBackground
        source={require('../images/fondo1.png')}
        style={styles.background}>
        <Text style={styles.text}>LOGIN</Text>

        <View style={StyleSheet.countContainer}>
          <Image source={require('../images/logo.png')} />
        </View>

        <Text style={styles.text}>User:</Text>

        <TextInput
          style={styles.textInput}
          id="id"
          onChangeText={text => this.changeUserInput(text)}
          value={this.state.user}
        />

        <Text style={styles.text}>Password:</Text>

        <TextInput
          style={styles.textInput}
          onChangeText={text => this.changePassInput(text)}
          value={this.state.password}
          secureTextEntry={true}
        />

        <Text style={{color: 'red', fontSize: 24}}>
          {this.state.loginMessage}
        </Text>

        <TouchableOpacity style={styles.button} onPress={this.login}>
          <Text>GO</Text>
        </TouchableOpacity>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  text: {
    padding: 10,
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
  },

  button: {
    top: 10,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  countContainer: {
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  },
  textInput: {
    height: 40,
    width: 200,
    borderColor: 'gray',
    borderWidth: 1,
    backgroundColor: 'white',
    borderRadius: 50,
    textAlign: 'center',
    opacity: 0.75,
  },
  background: {
    height: null,
    width: null,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    resizeMode: 'cover',
  },
});

export default LoginScreen;
