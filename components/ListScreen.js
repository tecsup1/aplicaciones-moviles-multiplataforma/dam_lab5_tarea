
import React,{Component} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Button,
    Alert,
    FlatList,
    Image,
  } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

// Componente item:
function Item({props,goDetails}){
    var hero_description = ''
    if(props.description == ''){
        hero_description = 'Este heroe no tiene descripción.';
    }else{
        hero_description = props.description;
    }

    return(
        <TouchableOpacity style={styles.item} onPress={()=>{goDetails(props)}}>

            <Image style={[styles.tinyLogo,{flex:1.6}]} source={{ uri: props.thumbnail.path+'/portrait_xlarge.jpg' }} />

            <View style={[styles.itemText,{flex:4}]}>
                <Text style={styles.title}>{props.name}</Text>
                <Text numberOfLines={2} style={styles.description}>{hero_description}</Text>
            </View>
            <Image style={{height:25,flex:1, alignSelf: 'center',}} source={require('../images/next.png')} />
        
        </TouchableOpacity>
    )
}


class ListScreen extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            loading:false,
            data:[],
        }
    }

    componentDidMount(){
        this.fetchData();
    }

    fetchData=()=>{
        this.setState({ loading:true })

        const apikey = '9e3d66957e41d7db7f364dfe7bb5497f';
        const ts = 1;
        const hash = 'b889f3b4144bc3d153cf268be106095c';
        
        const urlmarvel = `https://gateway.marvel.com:443/v1/public/characters?apikey=9e3d66957e41d7db7f364dfe7bb5497f&ts=1&hash=b889f3b4144bc3d153cf268be106095c&limit=35`
        
        const urlpokemon = 'https://pokeapi.co/api/v2/pokemon/?limit=20';

        fetch(urlmarvel,{
          method:'GET'
        })
        .then((res)=>res.json())
        .then((resJson)=>{
            //console.warn(resJson.data);
            this.setState({
                data: resJson.data.results,
                //data: resJson.results,
                loading:false,
            });
        })
        .catch((error)=>{
          console.warn(error)
        })
    }

    goDetails=(item)=>{
        this.props.navigation.navigate('DetailScreen',{
            item: item, 
        });
    }
 

    render() { 
        var mitexto = <Text>Todavía no se inicia la descarga</Text>

        if(this.state.loading){
            mitexto = <Text>Descargando...</Text>
        }else{
            mitexto = <Text>Archivos descargados</Text>
        }

        return ( 
            <View style={{ flex: 1 }}>
                        
                {/*mitexto*/}
              
                <FlatList 
                    data={this.state.data}
                    renderItem={
                        ({item})=><Item props={item} goDetails={this.goDetails}></Item>
                    }
                    ItemSeparatorComponent={
                        ()=>(
                            <View style={{ height: 1,
                            width: "100%",
                            backgroundColor: "#000", }}>
                            </View>
                        )
                    }
                />
               

            </View>
            
        );
    }
}
 
const styles = StyleSheet.create({

    item: {
      backgroundColor: 'white',
      padding: 10,
      marginVertical: 5,
      marginHorizontal: 16,
      flexDirection: 'row',
      borderRadius: 5,
    },
    itemText:{
        margin:10,
        color:'#4C5257',
        //Solución 1 para que texto no se salga de los limites
        //width: 0,
        //flexGrow: 1,
        //flex: 1,

        //Solución 2 para que texto no se salga de los limites
        flexShrink: 1
    },
    title: {
        fontSize: 20,
        color:'#4C5257',
        textShadowColor: 'black',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 12
    },
    tinyLogo: {
        width: 100,
        height: 100,
        borderRadius: 70,
    },
    description:{

    }
  });

export default ListScreen;