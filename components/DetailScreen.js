
import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Image,
  } from 'react-native';

class DetailScreen extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            item_received : this.props.route.params.item,
        }
    }
    render() { 
        return ( 
            <View style={{alignContent:'center',flex:1,alignItems:'center',}}>
                <Text style={styles.title}>{this.state.item_received.name}</Text>
                <Image style={styles.tinyLogo} source={{ uri: this.state.item_received.thumbnail.path+'/portrait_xlarge.jpg' }} />

                <Text style={styles.description}>{this.state.item_received.description}</Text>
                
            </View>
         );
    }
}
 

const styles = StyleSheet.create({

    item: {
      backgroundColor: 'skyblue',
      padding: 10,
      marginVertical: 5,
      marginHorizontal: 16,
      flexDirection: 'row',
      borderRadius: 5,
    },
    itemText:{
        margin:10,
        //Solución 1 para que texto no se salga de los limites
        //width: 0,
        //flexGrow: 1,
        //flex: 1,

        //Solución 2 para que texto no se salga de los limites
        flexShrink: 1
    },
    title: {
      fontSize: 20,
      color:'black',
        textShadowColor: 'black',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 12
    },
    tinyLogo: {
        width: 400,
        height: 400,
        borderRadius: 0,
        margin:15,
        
    },
    description:{
        marginHorizontal:20,
        marginVertical:10
    }
  });


export default DetailScreen;